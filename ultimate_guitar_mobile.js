// ==UserScript==
// @name         Ultimate Guitar Mobile Tweaks
// @namespace    https://ultimate-guitar.com/
// @version      0.1
// @description  Gain transpose and sharp to flat buttons on Mobile
// @author       Red
// @match        https://*.ultimate-guitar.com/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=ultimate-guitar.com
// @grant        GM_addStyle
// ==/UserScript==

// Hide annoying elements
GM_addStyle ( `
    .spl-fixed-action { display: none; }
    .js-chord-panel { display: none; }
    .ugm-b-section__strummings { display: none; }
    bidding-unit { display: none; }
` );


(function(){
    'use strict'

    // Remove the 'Open With' annoyance
    function alwaysInBrowser() {
        // removes the event listener on the tab options
        let old_element = document.getElementsByClassName("ugm-list")[0];
        let new_element = old_element.cloneNode(true);
        old_element.parentNode.replaceChild(new_element, old_element);
    }
    alwaysInBrowser();

    // Mobile only allows 3 tabs per day...
    function removeTabLimit() {
        let getWeekly = localStorage.getItem("tab_visit_count_weekly_v1");
        getWeekly = JSON.parse(getWeekly);
        getWeekly.count = 1;
        localStorage.setItem("tab_visit_count_weekly_v1", JSON.stringify(getWeekly));
    }
    removeTabLimit();

    // Next section is only for the tab page.
    if (window.location.host != "tabs.ultimate-guitar.com") {return}

    let stones = ['A', 'A#', 'B', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#'];
    let btones = ['A', 'Bb', 'B', 'C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab'];
    let tones = stones;
    // replace annoying notes
    let unwanted = ['E#', 'Fb', "B#", "Cb"];
    let wanted = ['F', 'E', 'C', "B"];

    // Select all chords
    let chords = document.getElementsByClassName("text-chord")

    // Add transpose and #tb buttons
    function addButtons() {
        document.getElementsByClassName("ugm-controls")[0].innerHTML = '<section><button style="padding: 10px; margin-right: 10px" id="transposen">-1</button><button id="transposep" style="padding: 10px">+1</button></section><section><button style="padding: 10px;" id="stft">#<>b</button></section>'
        document.getElementById("transposen").addEventListener("click", function () { transpose(-1); } );
        document.getElementById("transposep").addEventListener("click", function () { transpose(1); } );
        document.getElementById("stft").addEventListener("click", function () { stf(); } );
    }

    // Transpose all chords
    function transpose(amount) {
        let loc;
        let nnote;
        if (issharp) {tones = stones;} else {tones = btones;}
        for (let chord of chords) {
            chord.innerText.match(/([A-Z]#?b?)/g).forEach(
                onote => {
                    loc = tones.indexOf(onote);
                    nnote = tones[(loc+amount+12)%12];
                    chord.innerText = chord.innerText.replace(onote, nnote);
                })
        }
    }

    // Check if currently in sharp mode
    function checkifsharp() {
        if ((document.getElementsByClassName("js-tab-container")[0].innerHTML.match(/#/g) || []).length > 5) {
            return true;
        }
        return false;
    }

    // Translate sharps to flats or flats to sharps
    function stf() {
        let loc;
        let nnote;
        for (let chord of chords) {
            chord.innerText.match(/([A-G]#?b?)/g).forEach(
                onote => {
                    if (issharp) {
                        nnote = btones[stones.indexOf(onote)];
                    } else {
                        nnote = stones[btones.indexOf(onote)];
                    }
                    chord.innerText = chord.innerText.replace(onote, nnote);
                })
        }
        issharp = !issharp;
    }

    // Replace annoying notes
    function clean() {
        for (let chord of chords) {
            chord.innerText.match(/([A-G]#?b?)/g).forEach(
                onote => {
                    if (unwanted.indexOf(onote) >= 0) {
                        chord.innerText = chord.innerText.replace(onote, wanted[unwanted.indexOf(onote)] );
                    }
                })}
    }



    clean();
    addButtons();
    let issharp = checkifsharp();
})();

